# CFT Criteria Service

## About
Short term this service will be focused on Saved Searches (Criteria). Future this service will possibly include all Admin features for REL's (Rule Expression Language) whether its User or Report.

In the future, reporting will READ from Critiera Service rather than MySQL to construct the Critiera & Base Critiera for a search.

## Folder Layout

```yaml
# output directory for the tsconfig.json not checked in to git
bin
# configuration directory (see docs in the config folder)
config
  - config.ENV.json
  - pm2-ENV.json
# src directory for typescript (see docs in the typescript folder)
typescript
  # lib modules
  - app
    # folder for each module
    - MODULE_FOLDERS
    - inversify.config
    - interfaces.ts
    - types.ts
  # testing modules
  - test
# index.ts is for exporting a library
  - index.ts
# server.ts is for starting a long running service
  - server.ts
# tslint.json
  - tslint.json
.editorconfig
.gitignore
Gruntfile.js
package.json
tsconfig.json
```

## Setup
1. install all of the tools described in ../README.md
2. copy the entire folder to your new repo
3. configure the files to your needs

### package.json
This is the node package configuration as described on npm (link)

### tsconfig.json
This is our default tsconfig.json, it may need a minor change but overall should be similar. See docs on tsconfig at ms (link)

### .editorconfig
This is based off the global .editorconfig from workflow directory see link ()

### .gitignore
Straight forward git ignore file see info here (link)

### Gruntfile.js
Grunt is a task tool that helps build the typescript app into a usable js app using the typescript compiler.  It also runs common tasks like cleaning up, running tests and deploying.

#### Grunt Libraries
name | description
--|--
grunt-contrib-clean|
grunt-env|
grunt-mocha-cli|
grunt-shipit|
grunt-slack-webhook-plus|
grunt-ts|
grunt-tslint|
load-grunt-tasks|

## Configs
See the documentation under config/README.md

## Express Libraries
name | description
--|--
cft_smart-security | private security library
body-parser | json conversion
routing-controllers | express routing by decorators

## API
The following is the Saved Search API. Parameters and Options are specified for all REST calls. Options and parameters that are in **bold** are required.

All API routes can handle passing in credentials and account details either on the hook as a option or in the headers. For details on authentication please see the `npm-smart-security` module for details on how these are specified.

### Saved Search Routes

#### Requests
All routes can handle a BODY being with the following format:

```
{
   "userEmail": string,
   "accountCode": string,
   "title": string,
   "startDate": string,
   "endDate": string,
   "isDefault": boolean,
   "criteriaId": string,
   "schemaNamespace": string,
   "timeFrame": string,
   "adStatus": string,
   "fields":[
      {
         "name": string,
         "include": [
            string, ...
         ],
         "exclude": [
            string, ...
         ]
      }
   ]
}
```

The fields are:
* `userEmail` - string the user's email
* `accountCode` - string the active user account
* `title` - string friendly name for the criteria
* `isDefault` - boolean indicating this is the default for a given report
* `adStatus` - string identifying if you want `running` or `breaking` results
* `startDate` - string specifying the query start date formatted as MMDDYYYY
* `endDate` - string specifying the query end data formatted as MMDDYYYY
* `timeFrame` - string timeFrame duration using a mixture of a number and a type. Accepted types are w (week), m (month), and q (quarter). Example is 1w for 1 week or 2m for 2 months.
* `criteriaId` - report criteria identifier
* `schemaNamespace` - schema identifier

Finally, the following are request headers used to auth users:

* `x-ct-token`: the logged in user auth token
* `x-ct-account-id`: the account to use when processing requests
* `x-ct-user`: per `npm-smart-security` code, internal traffic uses `x-ct-user` for auth (when present, the `x-ct-internal` header is expected to be set)
* `x-ct-internal`: boolean value indicating that the caller is an internal user

#### Responses
When a route returns a payload that contains multiple saved searches, the response format is:

##### Full Response Payload

```
{
    "succeeded": boolean,
    "message": string,
    "result": [{
        "id": string,
        "userEmail": string,
        "accountCode": string,
        "criteriaId": string,
        "schemaId": string,
        "title": string,
        "isDefault": boolean,
        "adStatus": string,
        "startDate": string,
        "endDate": string,
        "timeFrame": string,
        "fields":[
            {
                 "name": string,
                 "include": [
                    string, "..."
                 ],
                 "exclude": [
                    string, "..."
                 ]
            }
        ]
    }],
    "paging": {
        "totalCount": number,
        "totalPages": number,
        "pageSize": number,
        "page": number
    }
}

```

The fields returned are:

* `succeeded`: will have a value of `true`
* `message`: will have a value of `Success`
* `result`: Either a single saved search (based on expectation) or a list of saved searches. The `id` is the saved search criteria identifier.
    * The description property is auto-generated.
* `paging`: Paging details
    * `totalCount`: total number of matching records
    * `totalPages`: total number of pages
    * `pageSize`: number of records returned
    * `page`: the page number

##### Partial Response Payload

```
{
    "succeeded": boolean,
    "message": string,
    "result": [{
        "id": string,
        "userEmail": string,
        "accountCode": string,
        "criteriaId": string,
        "schemaNamespace": string,
        "title": string,
        "isDefault": boolean
    }],
    "paging": {
        "totalCount": number,
        "totalPages": number,
        "pageSize": number,
        "page": number
    }
}

```

##### Failed Response Payload
```
{
    "succeeded": boolean,
    "message": error
}
```

The fields returned in failed responses are:
* `succeeded`: will have a value of `false`
* `message`: the error message

##### Create a new saved search criteria
* POST /criteria/api/v1/savedsearch
    * BODY: A payload serializing the search criteria to persist
    * RETURNS: A full payload containing the saved search criteria
    * For admins and authenticated users with access to the `smart_my_search` feature

##### Get an existing saved search criteria
* GET /criteria/api/v1/savedsearch/:id
    * **PARAMETER**: `id`, the search criteria identifier string
    * RETURNS: A full payload containing the saved search criteria
    * ERRORS:
        * 404
            * If a saved search for `criteriaId` is not found
            * If a non-admin user attempts to get data that is not theirs
    * For admins and authenticated users with access to the `smart_my_search` feature

##### Get all saved search criteria
* GET /criteria/api/v1/savedsearch
    * **FILTER ARG**: `userEmail`, optional user email string
    * **FILTER ARG**: `accountCode`, optional account identifier string
    * **FILTER ARG**: `criteriaId`, optional report criteria identifier string
    * **FILTER ARG**: `schemaNamespace`, optional schema identifier string
    * **FILTER ARG**: `isDefault`, optional boolean that indicates to just return the report default
    * **FILTER ARG**: `title`, optional string that is the friendly name for the criteria
    * **OPTION**: `page`, optional starting page number
    * **OPTION**: `pageSize`, optional number of records to return starting at `page`
    * RETURNS: A partial payload containing the saved search criteria
    * For admins and authenticated users with access to the `smart_my_search` feature

##### Update an existing saved search criteria
* PUT /criteria/api/v1/savedsearch/:id
    * **PARAMETER**: `id`, search criteria identifier string
    * BODY: A payload serializing the search criteria to persist
    * RETURNS: A full payload containing the updated search criteria
    * ERRORS:
        * 404
            * If a saved search for `criteriaId` is not found
            * If a non-admin user attempts to update data that is not theirs
    * For admins and authenticated users with access to the `smart_my_search` feature

##### Delete an existing saved search criteria
* DELETE /criteria/api/v1/savedsearch/:id
    * **PARAMETER**: `id`, search criteria identifier string
    * RETURNS: An HTTP 204 no content response on success
    * ERRORS:
        * 404
            * If a saved search with `criteriaId` is not found
            * If a non-admin user attempts to delete data that is not theirs
    * For admins and authenticated users with access to the `smart_my_search` feature
