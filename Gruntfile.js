module.exports = function (grunt) {

	// load all grunt tasks matching the ['grunt-*', '@*/grunt-*'] patterns
	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		env: {
			'debug': {
				NODE_ENV: 'debug',
				DEBUG: 'app:*,criteria:*'
			},
			'test': {
				NODE_ENV: 'test',
				DEBUG: 'app:*,criteria:*'
			}
		},
		tslint: {
			default: {
				options: {
					configuration: './typescript/tslint.json',
					fix: true
				},
				src: ['typescript/server.ts', 'typescript/app/**/*.ts']
			},
			tests: {
				options: {
					configuration: './typescript/test/tslint.json',
					fix: true
				},
				src: ['typescript/test/**/*.ts']
			}
		},
		ts: {
			'main': {
				tsconfig: true
			}
		},
		mochacli: {
			options: {
				globals: ['assert'],
				reporter: 'list',
				'check-leaks': true,
				bail: false
			},
			unit: {
				options: {
					require: ['bin/test/unit/globals.js']
				},
				src: ['bin/test/unit/**/*.js']
			},
			integration: {
				options: {
					require: ['bin/test/integration/globals.js']
				},
				src: ['bin/test/integration/**/*.js']
			},
			system: {
				options: {
					require: ['test/system/globals.js']
				},
				src: ['test/system/**/*.js']
			}
		},
		watch: {
			scripts: {
				files: ['Gruntfile.js', 'typescript/**/*.ts'],
				tasks: ['default'],
				options: {
					atBegin: true
				}
			}
		},
		clean: {
			bin: ["./bin"]
		}
	});

	grunt.task.run('gitinfo');

	grunt.registerTask('default', [
		'clean:bin', 'tslint:default', 'ts:main',
		'tslint:tests', 'env:test', 'mochacli:unit', 'mochacli:integration'
	]);

	grunt.registerTask('test', 'default');

	grunt.registerTask('testAll', [
		'test', 'mochacli:system'
	]);
};
