// This is a library export and it will export services / types / interfaces that can be used from outside this repository

// Always import the inversify config first
import {container} from './app/inversify.config';

export {container};
