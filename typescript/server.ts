#!/usr/bin/env ts-node
/// <reference path="../node_modules/cft-type-definitions/index.d.ts" />
/// <reference path="../node_modules/cft-type-definitions/express-typings.d.ts" />

// This should be the first import to any code that is executable as its a shim that is global and provides state to other modules
import 'reflect-metadata';

// Set up source map
import sourceMapSupport = require('source-map-support');
sourceMapSupport.install();

// Setup spm for production
if (process.env.NODE_ENV === 'prod') {
	// tslint:disable-next-line:no-var-requires
	require('spm-agent-nodejs');
}

// Always import inversify-container before any other modules in app
import * as cors from 'cors';
import * as express from 'express';
import * as _ from 'lodash';
import {container} from './app/inversify.config';

// import all modules for the express app
import {useContainer, useExpressServer} from 'routing-controllers';
import {ErrorHandlerMiddleware} from './app/middleware/ErrorHandlerMiddleware';
import {healthCheck} from './app/middleware/HealthCheckMiddleware';
import {LoggingMiddleware} from './app/middleware/LoggingMiddleware';
import {SmartSecurityMiddleware} from './app/middleware/SmartSecurityMiddleware';
import {SavedSearchController} from './app/search/controller/SavedSearchController';
import {LoggerFactory} from './app/utils/LoggerFactory';
import {MongooseConnection} from './app/utils/MongooseConnection';

const log = LoggerFactory.getOrCreateLogger('server');

MongooseConnection.create().then(() => {
	// Tell pm2 we're ready after we have a connection.
	if (_.isFunction(process.send)) {
		log.info('server -- MongooseConnection created, sending pm2 ready event');
		process.send('ready');
	}
});

useContainer(container);

const expressApp = express();

expressApp.use(cors({
	allowedHeaders: ['Access-Control-Allow-Headers', 'Content-Type', 'X-Requested-With', 'Accept',
		'OriginAccess-Control-Request-Method', 'Access-Control-Request-Headers', 'X-CT-Token',
		'X-CT-Account-Id', 'X-CT-DataSource-Id', 'Request-Id'],
	optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}));

expressApp.get('/configuration/health', healthCheck);

// Create express app
useExpressServer(expressApp, {
	defaultErrorHandler: false,
	routePrefix: '/criteria/api',
	controllers: [SavedSearchController],
	middlewares: [
		LoggingMiddleware,
		SmartSecurityMiddleware,
		ErrorHandlerMiddleware
	]
});

const DEFAULT_PORT = 5080;
expressApp.listen(process.env.PORT || DEFAULT_PORT);
