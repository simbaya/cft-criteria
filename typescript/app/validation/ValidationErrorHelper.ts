import { ValidationError } from 'class-validator';
import * as _ from 'lodash';

export class ValidationErrorHelper {
	public static loopThroughErrors = (validationErrors: ValidationError[], errorMessages: string[]) => {
		_.forEach(validationErrors, (validationError: ValidationError) => {
			if (validationError.constraints) {
				_.forEach(validationError.constraints, (value: string) => {
					errorMessages.push(`Property ${value}`);
				});
			}
			if (validationError.children) {
				ValidationErrorHelper.loopThroughErrors(validationError.children, errorMessages);
			}
		});
		return errorMessages;
	}

	public static buildValidationMessage = (validationErrors: ValidationError[]): string => {
		const errors: string[] = [];
		return `ValidationError: ${ValidationErrorHelper.loopThroughErrors(validationErrors, errors).join(', ')}`;
	}
}
