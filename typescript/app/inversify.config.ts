import {Container, inject} from 'inversify';
import { makeProvideDecorator } from 'inversify-binding-decorators';

const container = new Container();

const Provide = makeProvideDecorator(container);

export {container, Provide, inject};
