import * as express from 'express';
import {SmartResponse} from '../models/SmartResponse';

export function healthCheck(req: express.Request, res: express.Response): void {
	const message: string = 'Successfully called the health check';
	const result: string = 'healthy';
	res.send(new SmartResponse(result, message));
}
