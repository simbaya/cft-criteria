import SmartSecurity = require('cft-smart-security');
import * as express from 'express';
import { ExpressMiddlewareInterface, Middleware } from 'routing-controllers';
import { Provide } from '../inversify.config';

@Provide(SmartSecurityMiddleware)
@Middleware({ type: 'before' })
export class SmartSecurityMiddleware implements ExpressMiddlewareInterface {

	// Set the settings for Smart Security required for you application.
	public static checkMySearchAccess: string = 'smart_my_search';
	public static includeFeatures: boolean = true;
	public static includeAccounts: boolean = true;

	public use: express.RequestHandler = SmartSecurity.AuthenticationHandler(
		SmartSecurityMiddleware.includeAccounts,
		SmartSecurityMiddleware.includeFeatures,
		SmartSecurityMiddleware.checkMySearchAccess
	) as express.RequestHandler;
}
