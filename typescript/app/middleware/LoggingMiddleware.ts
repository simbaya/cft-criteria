import { requestLoggingMiddleware } from 'cft-logging';
import * as express from 'express';
import { ExpressMiddlewareInterface, Middleware } from 'routing-controllers';
import { Provide } from '../inversify.config';

@Provide(LoggingMiddleware)
@Middleware({ type: 'before' })
export class LoggingMiddleware implements ExpressMiddlewareInterface {
	public use: express.RequestHandler = requestLoggingMiddleware() as express.RequestHandler;
}
