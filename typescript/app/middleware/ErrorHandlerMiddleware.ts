import {ValidationError} from 'class-validator';
import * as express from 'express';
import {ExpressErrorMiddlewareInterface, Middleware} from 'routing-controllers';
import {Provide} from '../inversify.config';
import {RequestError} from '../models/errors/RequestError';
import {SmartResponse} from '../models/SmartResponse';
import {LoggerFactory} from '../utils/LoggerFactory';
import {ValidationErrorHelper} from '../validation/ValidationErrorHelper';

const log = LoggerFactory.getOrCreateLogger('middleware' );

@Middleware({ type: 'after' })
@Provide(ErrorHandlerMiddleware)
export class ErrorHandlerMiddleware implements ExpressErrorMiddlewareInterface {
	public error(
		err: {message: string, status?: number, errors?: ValidationError[], rootCause?: Error},
		request: express.Request,
		response: express.Response,
		next: express.NextFunction
	): void {
		if (!err) {
			err = new RequestError('Unknown Error');
		}

		let responseMessage: string = err.message;

		if (err.errors) {
			responseMessage = ValidationErrorHelper.buildValidationMessage(err.errors);
			response.status(403);
		} else if (err.status) {
			response.status(err.status);
		} else {
			response.status(500);
		}

		log.error(err);

		response.send(new SmartResponse(null, responseMessage));
		next();
	}
}
