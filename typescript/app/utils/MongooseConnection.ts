import {Config, ServerConfig} from 'cft-smart-security';
import _ = require('lodash');
import mongoose = require('mongoose');
import {LoggerFactory} from './LoggerFactory';
import {ShutdownHandler} from './ShutdownHandler';

const log = LoggerFactory.getOrCreateLogger('mongoose');

export interface IMongooseConnectionOptions {
	connectTimeoutMS: number;
	keepAlive: number;
	poolSize: number;
	isDebug: boolean;
}

const defaultOptions:IMongooseConnectionOptions = {
	connectTimeoutMS: 30000, // 30 seconds,
	keepAlive: 300000, // 300 seconds -- 5 minutes
	poolSize: 10,
	isDebug: true
};

export class MongooseConnection {

	public static create(options?: Partial<IMongooseConnectionOptions>):Promise<void> {
		return new Promise<void>(async (resolve, reject): Promise<void> => {

			options = options || {};

			const mergedOptions: IMongooseConnectionOptions = _.extend({}, defaultOptions, options);

			// connect to Mongo during startup
			const mongoConfigs: ServerConfig[] = Config.getServerConfigs('mongodb');

			const mongoConnectionStrings: string[] = [];
			// Get all the servers and build their connection strings.
			_.forEach(mongoConfigs, (mongoConfig: ServerConfig) => {
				mongoConnectionStrings.push(mongoConfig.getHost() + ':' + mongoConfig.getPort() + mongoConfig.getPath());
			});
			const mongooseUrl = 'mongodb://' + mongoConnectionStrings.join(',');

			const mongooseOptions = {
				server: {
					autoReconnect: true,
					connectTimeoutMS: mergedOptions.connectTimeoutMS,
					keepAlive: mergedOptions.keepAlive,
					poolSize: mergedOptions.poolSize
				}
			};

			// Use ES6 Promises.
			// @see http://mongoosejs.com/docs/promises.html
			mongoose.Promise = global.Promise;
			mongoose.connect(mongooseUrl, mongooseOptions);
			mongoose.set('debug', mergedOptions.isDebug);

			mongoose.connection.on('connecting', (): void => {
				log.info('MongooseConnection: Mongoose connecting.');
			});

			mongoose.connection.on('connected', (): void => {
				ShutdownHandler.registerAppShutdown('MongooseConnection:shutdown', (): Promise<void> => {
					return new Promise<void>((appShutdownResolve, appShutdownReject): void => {
						mongoose.connection.close((): void => {
							log.info('MongooseConnection: Mongoose connection disconnected through app termination');
							appShutdownResolve();
						});
					});
				});
				resolve();
				log.info('MongooseConnection: Mongoose connection connected.');
			});

			mongoose.connection.on('open', (): void => {
				log.info('MongooseConnection: Mongoose connection open.');
			});

			mongoose.connection.on('reconnected', (): void => {
				log.info('MongooseConnection: Mongoose reconnected.');
			});

			mongoose.connection.on('error', (err: Error): void => {
				log.info('MongooseConnection: Mongoose connection error: ' + err);
			});

			mongoose.connection.on('disconnected', (): void => {
				log.info('MongooseConnection: Mongoose connection disconnected');
			});
		});
	}
}
