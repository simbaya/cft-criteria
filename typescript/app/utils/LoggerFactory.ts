import {ILogger, Logger} from 'cft-logging';

export class LoggerFactory {
	public static getOrCreateLogger(name: string): ILogger {
		return Logger.getOrCreate().child({ debug: `criteria:${name.toLowerCase()}` });
	}
}
