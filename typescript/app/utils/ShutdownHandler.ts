import _ = require('lodash');
import Q = require('q');

import {LoggerFactory} from './LoggerFactory';

const log = LoggerFactory.getOrCreateLogger('shutdown');

/**
 * One class to try to unify all the shutdown tasks.
 * Currently grouped into 2 levels, user level and app level.
 * user level = specific classes/cleanup for a user
 * app level = Shared resources (mongo, etc).
 */
export type ShutdownFunction = () => Promise<any>;

export type UnRegisterFunction = () => void;

class ShutdownHandlerClass {

	private userLevelShutdownFunctions:{[name:string]:ShutdownFunction} = {};
	private appLevelShutdownFunctions:{[name:string]:ShutdownFunction} = {};

	constructor() {
		log.info('ShutdownHandler:constructor -- Register process events');
		process.on('SIGINT', () => {
			log.info('ShutdownHandler:SIGINT');
			this.shutdown();
		});
		process.on('SIGTERM', () => {
			log.info('ShutdownHandler:SIGTERM');
			this.shutdown();
		});
		process.on('uncaughtException', (error:Error) => {
			// tslint:disable-next-line
			console.trace('ShutdownHandler:Uncaught Exception', error);
			this.shutdown(true);
		});
	}

	public registerUserShutdown(name:string, shutdownFunction:ShutdownFunction):UnRegisterFunction {
		log.info('ShutdownHandler:registerUserShutdown - ' + name);
		this.userLevelShutdownFunctions[name] = shutdownFunction;

		return () => {
			this.unregisterUserShutdown(name);
		};
	}

	public registerAppShutdown(name:string, shutdownFunction:ShutdownFunction):UnRegisterFunction {
		log.info('ShutdownHandler:registerAppShutdown - ' + name);
		this.appLevelShutdownFunctions[name] = shutdownFunction;

		return () => {
			this.unregisterAppShutdown(name);
		};
	}

	private unregisterUserShutdown(name:string):void {
		delete this.userLevelShutdownFunctions[name];
	}

	private unregisterAppShutdown(name:string):void {
		delete this.appLevelShutdownFunctions[name];
	}

	private shutdown(isError:boolean = false):void {

		const code:number = (isError) ? 1 : 0;

		const userPromises:Array<Promise<any>> = [];

		_.forEach(this.userLevelShutdownFunctions, (shutdownFunction:ShutdownFunction, name:string):void => {
			log.info('ShutdownHandler:shutdown -- User Level -- ' + name);
			userPromises.push(shutdownFunction());
		});

		// We use Q so we can get the allSettled, which waits until everything is complete, even if one rejects.
		Q.allSettled(userPromises).then((userPromiseStates:Array<Q.PromiseState<any>>) => {

			this.logRejectedPromises(userPromiseStates);
			log.info('ShutdownHandler:shutdown -- All user promises settled.');

			const appPromises:Array<Promise<any>> = [];

			_.forEach(this.appLevelShutdownFunctions, (shutdownFunction:ShutdownFunction, name:string):void => {
				log.info('ShutdownHandler:shutdown -- App Level -- ' + name);
				appPromises.push(shutdownFunction());
			});

			Q.allSettled(appPromises).then((appPromiseStates:Array<Q.PromiseState<any>>) => {
				this.logRejectedPromises(appPromiseStates);
				log.info('ShutdownHandler:shutdown -- All app promises settled., exiting');
				process.exit(code);
			}).fail((error:Error) => {
				log.info('ShutdownHandler:shutdown -- App Promise Shutdown Failure', error);
			});

		}).fail((error:Error) => {
			log.info('ShutdownHandler:shutdown -- User Promise Shutdown Failure', error);
		});

	}

	private logRejectedPromises(promiseStates:Array<Q.PromiseState<any>>):void {
		_.forEach(promiseStates, (userPromise:Q.PromiseState<any>) => {
			if (userPromise.state === 'rejected') {
				log.info('ShutdownHandler:logRejectedPromises -- Promise Rejected', userPromise);
			}
		});
	}
}

export const ShutdownHandler:ShutdownHandlerClass = new ShutdownHandlerClass();
