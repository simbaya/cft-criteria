import {PageMetadata} from './PageMetadata';

export class PagedResult<T> {
	private readonly _result: T;
	private readonly _pageMetadata: PageMetadata;

	constructor(result: T, pageMetadata: PageMetadata) {
		this._result = result;
		this._pageMetadata = pageMetadata;
	}

	public get result(): T {
		return this._result;
	}

	public get pageMetadata(): PageMetadata {
		return this._pageMetadata;
	}
}
