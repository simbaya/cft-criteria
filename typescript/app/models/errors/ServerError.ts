import {RequestError} from './RequestError';

export class ServerError extends RequestError {
	constructor(originalError: Error) {
		super('A server error occurred', 500, originalError);
	}
}
