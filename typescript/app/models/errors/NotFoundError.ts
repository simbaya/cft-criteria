import {RequestError} from './RequestError';

export class NotFoundError extends RequestError {
	constructor(id: string) {
		super(`Document with id ${id} not found`, 404);
	}
}
