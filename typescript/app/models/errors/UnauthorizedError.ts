import {RequestError} from './RequestError';

export class UnauthorizedError extends RequestError {
	constructor() {
		super('Unable to perform the requested action', 404);
	}
}
