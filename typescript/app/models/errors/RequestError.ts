export class RequestError extends Error {
	public status: number;
	public originalError: Error;

	constructor(message: string, status: number = 500, originalError?: Error) {
		super(message);
		this.status = status;
		this.originalError = originalError;
	}
}
