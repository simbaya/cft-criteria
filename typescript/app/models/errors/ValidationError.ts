import {RequestError} from './RequestError';

export class ValidationError extends RequestError {
	constructor(errorMessage: string) {
		super(`ValidationError - ${errorMessage}`, 403);
	}
}
