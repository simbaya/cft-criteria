import {PagedResult} from './PagedResult';
import {PageMetadata} from './PageMetadata';

export interface ISmartResponse<T> {
	succeeded: boolean;
	message?: string;
	result?: T;
	paging?:  PageMetadata;
}

export class SmartResponse<T> implements ISmartResponse<T> {
	public succeeded: boolean = false;
	public message: string = null;
	public result: T = null;
	public paging:  PageMetadata = null;

	constructor(result?: T | PagedResult<T>, message: string = null) {
		this.message = message;

		if (result) {
			this.succeeded = true;

			if (result instanceof PagedResult) {
				this.result = result.result;
				this.paging = result.pageMetadata;
			} else {
				this.result = result;
			}
		}
	}
}
