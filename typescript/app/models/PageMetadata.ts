export class PageMetadata {
	public readonly totalCount: number;
	public readonly totalPages: number;
	public readonly pageSize: number;
	public readonly page: number;

	public constructor(page: number, pageSize: number, totalCount: number) {
		this.page = page;
		this.pageSize = pageSize;
		this.totalCount = totalCount;
		this.totalPages = (totalCount > 0) ? Math.ceil(totalCount / pageSize) : 0;
	}
}
