import * as mongoose from 'mongoose';
import {Provide} from '../../inversify.config';
import {ISavedSearchDocument, SavedSearchModel} from './SavedSearchModel';

export const SavedSearchModelFactoryType = Symbol('SavedSearchModelFactory');

export interface ISavedSearchModelFactory {
	getSavedSearchModel(): mongoose.Model<ISavedSearchDocument>;
}

@Provide(SavedSearchModelFactoryType)
export class SavedSearchModelFactory implements  ISavedSearchModelFactory {
	public getSavedSearchModel(): mongoose.Model<ISavedSearchDocument> {
		return SavedSearchModel;
	}
}
