import {plainToClass} from 'class-transformer';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import {ISavedSearch} from './ISavedSearch';
import {ISavedSearchPartial} from './ISavedSearchPartial';
import {SavedSearch} from './SavedSearch';
import {SavedSearchPartial} from './SavedSearchPartial';

function toDate(dateString: string): Date {
	if (_.isEmpty(dateString)) {
		return null;
	}

	return moment(dateString, 'MMDDYYYY').toDate();
}

function toDateString(date: Date): string {
	if (_.isNil(date)) {
		return null;
	}

	return moment(date).format('MMDDYYYY');
}

const SavedSearchSchema: mongoose.Schema = new mongoose.Schema({
	userEmail: {type: String, index: {unique: false}},
	accountCode: {type: String, index: {unique: false}},
	criteriaId: {type: String, index: {unique: false}},
	schemaNamespace: {type: String, index: {unique: false}},
	title: {type: String},
	isDefault: {type: Boolean},
	startDate: {type: Date, get: toDateString, set: toDate},
	endDate: {type: Date, get: toDateString, set: toDate},
	timeFrame: {type: String},
	adStatus: {type: String},
	fields: {type: Array}
});

// Cannot use arrow functions because Mongoose sets 'this' to reference the model instance
/* tslint:disable:only-arrow-functions */
SavedSearchSchema.methods.toSavedSearch = function(this: ISavedSearchDocument): ISavedSearch {
	return plainToClass<SavedSearch, ISavedSearch>(SavedSearch, {
		id: this._id.toString(),
		userEmail: this.userEmail,
		accountCode: this.accountCode,
		criteriaId: this.criteriaId,
		schemaNamespace: this.schemaNamespace,
		title: this.title,
		isDefault: this.isDefault,
		startDate: this.startDate,
		endDate: this.endDate,
		timeFrame: this.timeFrame,
		adStatus: this.adStatus,
		fields: this.fields
	});
};

SavedSearchSchema.methods.toSavedSearchPartial = function(this: ISavedSearchDocument): ISavedSearchPartial {
	return plainToClass<SavedSearchPartial, ISavedSearchPartial>(SavedSearchPartial, {
		id: this._id.toString(),
		userEmail: this.userEmail,
		accountCode: this.accountCode,
		criteriaId: this.criteriaId,
		schemaNamespace: this.schemaNamespace,
		title: this.title,
		isDefault: this.isDefault
	});
};
/* tslint:enable:only-arrow-functions */

export interface ISavedSearchDocument extends ISavedSearch, mongoose.Document {
	toSavedSearch(): ISavedSearch;
	toSavedSearchPartial(): ISavedSearchPartial;
}

export const SavedSearchModel = mongoose.model<ISavedSearchDocument>('SavedSearch', SavedSearchSchema, 'savedSearches');
