import {Type} from 'class-transformer';
import {IsBoolean, IsEmail, IsOptional, IsString} from 'class-validator';

export class SavedSearchFilter {
	@IsOptional()
	@IsEmail()
	public userEmail: string = null;

	@IsOptional()
	@IsString()
	public accountCode: string = null;

	@IsOptional()
	@IsString()
	public title: string = null;

	@IsOptional()
	@IsBoolean()
	@Type(() => Boolean)
	public isDefault: boolean = null;

	@IsOptional()
	@IsString()
	public criteriaId: string = null;

	@IsOptional()
	@IsString()
	public schemaNamespace: string = null;
}
