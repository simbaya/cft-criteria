export interface ISearchField {
	name: string;
	include: string[];
	exclude: string[];
}
