import {Transform, Type} from 'class-transformer';
import {IsNotEmpty, IsOptional, Matches, ValidateIf, ValidateNested} from 'class-validator';
import * as _ from 'lodash';
import * as moment from 'moment';
import {SavedSearchPartial} from './SavedSearchPartial';
import {SearchField} from './SearchField';

export class SavedSearch extends SavedSearchPartial {
	@ValidateIf((o) => _.isEmpty(o.timeFrame))
	@IsNotEmpty()
	@Transform((value) => _.isDate(value) ? moment(value).format('MMDDYYYY') : value)
	public startDate: string = null;

	@ValidateIf((o) => _.isEmpty(o.timeFrame))
	@IsNotEmpty()
	@Transform((value) => _.isDate(value) ? moment(value).format('MMDDYYYY') : value)
	public endDate: string = null;

	@ValidateIf((o) => _.isEmpty(o.startDate) && _.isEmpty(o.endDate))
	@Matches(new RegExp(/(today|yesterday|last 7 days|last 14 days|last month|last 3 months|last 6 months|year to date|last year)/i))
	public timeFrame: string = null;

	@IsOptional()
	@Matches(new RegExp(/(breaking|running)/i))
	public adStatus: string = null;

	@IsOptional()
	@Type(() => SearchField)
	@ValidateNested()
	public fields: SearchField[] = null;
}
