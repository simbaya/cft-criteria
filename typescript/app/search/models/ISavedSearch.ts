import {ISavedSearchPartial} from './ISavedSearchPartial';
import {ISearchField} from './ISearchField';

export interface ISavedSearch extends ISavedSearchPartial {
	startDate: string;
	endDate: string;
	timeFrame: string;
	adStatus: string;
	fields: ISearchField[];
}
