export interface ISavedSearchPartial {
	id?: string;
	userEmail: string;
	accountCode: string;
	title: string;
	isDefault: boolean;
	criteriaId: string;
	schemaNamespace: string;
}
