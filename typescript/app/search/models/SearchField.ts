import {IsDefined, IsOptional, IsString} from 'class-validator';
import {ISearchField} from './ISearchField';

export class SearchField implements ISearchField {
	@IsDefined()
	@IsString()
	public name: string;

	@IsOptional()
	@IsString({each: true})
	public include: string[] = null;

	@IsOptional()
	@IsString({each: true})
	public exclude: string[] = null;
}
