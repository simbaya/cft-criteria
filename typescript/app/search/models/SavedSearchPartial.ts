import {IsBoolean, IsDefined, IsEmail, IsNotEmpty, IsOptional, IsString} from 'class-validator';
import {ISavedSearchPartial} from './ISavedSearchPartial';

export class SavedSearchPartial implements  ISavedSearchPartial {
	@IsOptional()
	@IsString()
	public id?: string = null;

	@IsDefined()
	@IsString()
	@IsEmail()
	public userEmail: string = null;

	@IsDefined()
	@IsString()
	@IsNotEmpty()
	public accountCode: string = null;

	@IsDefined()
	@IsString()
	@IsNotEmpty()
	public title: string = null;

	@IsDefined()
	@IsBoolean()
	public isDefault: boolean = false;

	@IsDefined()
	@IsString()
	@IsNotEmpty()
	public criteriaId: string = null;

	@IsDefined()
	@IsString()
	@IsNotEmpty()
	public schemaNamespace: string = null;
}
