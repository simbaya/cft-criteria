import {IUser} from 'cft-smart-security';
import {Validator} from 'class-validator';
import {Request} from 'express';
import * as _ from 'lodash';
import {Body, Delete, Get, JsonController, Param, Post, Put, QueryParam, Req} from 'routing-controllers';
import {inject, Provide} from '../../inversify.config';
import {NotFoundError, UnauthorizedError, ValidationError} from '../../models/errors';
import {SmartResponse} from '../../models/SmartResponse';
import {LoggerFactory} from '../../utils/LoggerFactory';
import {ISavedSearch} from '../models/ISavedSearch';
import {ISavedSearchPartial} from '../models/ISavedSearchPartial';
import {SavedSearch} from '../models/SavedSearch';
import {SavedSearchFilter} from '../models/SavedSearchFilter';
import {ISavedSearchService} from '../service/ISavedSearchService';
import {SavedSearchServiceType} from '../service/SavedSearchService';
import {ISavedSearchController} from './ISavedSearchController';

const log = LoggerFactory.getOrCreateLogger('controller');
const validator = new Validator();

@JsonController('/v1/savedsearch')
@Provide(SavedSearchController)
export class SavedSearchController implements ISavedSearchController {
	private savedSearchService: ISavedSearchService;

	constructor(@inject(SavedSearchServiceType) savedSearchService: ISavedSearchService) {
		this.savedSearchService = savedSearchService;
	}

	@Post('/')
	public async create(
		@Body({ required: true }) savedSearch: SavedSearch,
		@Req() request: Request
	): Promise<SmartResponse<ISavedSearch>> {
		const requestId = request.requestId;

		log.debug(`Creating saved search for ${requestId}`);

		if (!_.isEmpty(savedSearch.id)) {
			throw new ValidationError('The object should not have an id');
		}

		this.validateEmailsMatchExceptForAdmin(request.authenticatedUser, savedSearch);

		const newSavedSearch = await this.savedSearchService.create(savedSearch);

		log.debug(`Finished creating saved search for ${requestId}`);

		return new SmartResponse(newSavedSearch);
	}

	@Get('/:id')
	public async getById(
		@Param('id') id: string,
		@Req() request: Request
	): Promise<SmartResponse<ISavedSearch>> {
		const requestId = request.requestId;

		log.debug(`Getting saved search with id:${id} for ${requestId}`);

		this.validateId(id);

		const savedSearch = await this.savedSearchService.findById(id);

		this.validateExists(id, savedSearch);
		this.validateEmailsMatchExceptForAdmin(request.authenticatedUser, savedSearch);

		log.debug(`Finished getting saved search with id:${id} for ${requestId}`);

		return new SmartResponse(savedSearch);
	}

	@Put('/:id')
	public async update(
		@Param('id') id: string,
		@Body({ required: true }) savedSearch: SavedSearch,
		@Req() request: Request
	): Promise<SmartResponse<ISavedSearch>> {
		const requestId = request.requestId;

		log.debug(`Updating saved search with id:${id} for ${requestId}`);

		this.validateId(id);
		this.validateIdsMatch(id, savedSearch.id);
		this.validateEmailsMatchExceptForAdmin(request.authenticatedUser, savedSearch);

		const existingSavedSearch = await this.savedSearchService.findById(id);

		this.validateExists(id, existingSavedSearch);
		this.validateEmailsMatchExceptForAdmin(request.authenticatedUser, existingSavedSearch);

		const updatedSavedSearch = await this.savedSearchService.update(savedSearch);

		log.debug(`Finished updating saved search with id:${id} for ${requestId}`);

		return new SmartResponse(updatedSavedSearch);
	}

	@Delete('/:id')
	public async deleteById(
		@Param('id') id: string,
		@Req() request: Request
	): Promise<SmartResponse<ISavedSearch>> {
		const requestId = request.requestId;

		log.debug(`Deleting saved search with id:${id} for ${requestId}`);

		this.validateId(id);

		const savedSearch = await this.savedSearchService.findById(id);

		this.validateExists(id, savedSearch);
		this.validateEmailsMatchExceptForAdmin(request.authenticatedUser, savedSearch);

		await this.savedSearchService.deleteById(id);

		log.debug(`Finished deleting saved search with id:${id} for ${requestId}`);

		return new SmartResponse(savedSearch);
	}

	@Get('/')
	public async findByFilter(
		@QueryParam('filter', {required: true}) filter: SavedSearchFilter,
		@QueryParam('page') page: number,
		@QueryParam('pageSize') pageSize: number,
		@Req() request: Request
	): Promise<SmartResponse<ISavedSearchPartial[]>> {
		const requestId = request.requestId;

		log.debug(`Finding saved searches for ${requestId} matching filter args ${filter}`);

		this.validatePaging(page, pageSize);
		this.validateEmailsMatchExceptForAdmin(request.authenticatedUser, filter);

		const pagedResult = await this.savedSearchService.findByFilter(filter, page, pageSize);

		log.debug(`Finished finding saved searches for ${requestId} matching filter args ${JSON.stringify(filter)}`);

		return new SmartResponse(pagedResult);
	}

	private validateId(id: string): void {
		if (_.isEmpty(id)) {
			throw new ValidationError('id parameter should not be empty');
		}

		// The @IsMongoId attribute doesn't work on strings so regex is used instead
		if (!/^[a-zA-Z0-9-]+$/.test(id)) {
			throw new ValidationError(`${id} is not a valid id`);
		}
	}

	private validatePaging(page?: number, pageSize?: number): void {
		const errorMessage = 'page and pageSize parameters must be numeric and greater than 0';

		if ((!_.isNil(page) && !validator.isNumber(page)) || page < 1) {
			throw new ValidationError(errorMessage);
		}

		if ((!_.isNil(pageSize) && !validator.isNumber(pageSize)) || pageSize < 1) {
			throw new ValidationError(errorMessage);
		}
	}

	private validateEmailsMatchExceptForAdmin(user: IUser, {userEmail}: {userEmail?: string}): void {
		if (!user.isAdmin && !_.isEqual(_.lowerCase(user.email), _.lowerCase(userEmail))) {
			throw new UnauthorizedError();
		}
	}

	private validateIdsMatch(paramId: string, bodyId: string): void {
		if (!_.isEqual(paramId, bodyId)) {
			throw new ValidationError('The ids in the URL and body must match');
		}
	}

	private validateExists(id: string, savedSearch: ISavedSearch): void {
		if (_.isEmpty(savedSearch)) {
			throw new NotFoundError(id);
		}
	}
}
