import {Request} from 'express';
import {SmartResponse} from '../../models/SmartResponse';
import {ISavedSearch} from '../models/ISavedSearch';
import {ISavedSearchPartial} from '../models/ISavedSearchPartial';
import {SavedSearch} from '../models/SavedSearch';
import {SavedSearchFilter} from '../models/SavedSearchFilter';

export interface ISavedSearchController {
	create(savedSearch: SavedSearch, request: Request): Promise<SmartResponse<ISavedSearch>>;

	getById(id: string, request: Request): Promise<SmartResponse<ISavedSearch>>;

	deleteById(id: string, request: Request): Promise<SmartResponse<ISavedSearch>>;

	findByFilter(filter: SavedSearchFilter, page: number, pageSize: number, request: Request): Promise<SmartResponse<ISavedSearchPartial[]>>;

	update(id: string, savedSearch: SavedSearch, request: Request): Promise<SmartResponse<ISavedSearch>>;
}
