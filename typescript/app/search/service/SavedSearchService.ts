import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import {inject, Provide} from '../../inversify.config';
import {ServerError} from '../../models/errors/ServerError';
import {PagedResult} from '../../models/PagedResult';
import {PageMetadata} from '../../models/PageMetadata';
import {LoggerFactory} from '../../utils/LoggerFactory';
import {ISavedSearch} from '../models/ISavedSearch';
import {ISavedSearchPartial} from '../models/ISavedSearchPartial';
import {SavedSearchFilter} from '../models/SavedSearchFilter';
import {ISavedSearchDocument} from '../models/SavedSearchModel';
import {ISavedSearchModelFactory, SavedSearchModelFactoryType} from '../models/SavedSearchModelFactory';
import {SavedSearchPartial} from '../models/SavedSearchPartial';
import {ISavedSearchService} from './ISavedSearchService';

const log = LoggerFactory.getOrCreateLogger('service');
export const SavedSearchServiceType = Symbol('SavedSearchService');

@Provide(SavedSearchServiceType)
export class SavedSearchService implements ISavedSearchService {
	private readonly SavedSearchModel: mongoose.Model<ISavedSearchDocument>;

	constructor(@inject(SavedSearchModelFactoryType) modelFactory: ISavedSearchModelFactory) {
		this.SavedSearchModel = modelFactory.getSavedSearchModel();
	}

	public async findById(id: string): Promise<ISavedSearch> {
		try {
			const savedSearchDocument = await this.SavedSearchModel.findById(id).exec();

			if (_.isEmpty(savedSearchDocument)) {
				return Promise.resolve(null);
			}

			return Promise.resolve(savedSearchDocument.toSavedSearch());
		} catch (error) {
			return Promise.reject(new ServerError(error));
		}
	}

	public async findByFilter(
		filter: SavedSearchFilter,
		page: number = 1,
		pageSize: number = 20
	): Promise<PagedResult<ISavedSearchPartial[]>> {
		const conditions = _.omitBy(filter, _.isNil);
		const countQuery = this.SavedSearchModel.count(conditions);
		const findQuery = this.SavedSearchModel
			.find(conditions)
			.select(_.keys(new SavedSearchPartial()).join(' '))
			.sort({_id: 1})
			.skip((page - 1) * pageSize)
			.limit(pageSize);

		try {
			const results = await Promise.all([findQuery.exec(), countQuery.exec()]);

			if (_.isEmpty(results[0])) {
				return Promise.resolve(new PagedResult([], new PageMetadata(page, 0, results[1])));
			}

			const jsonObjects = _.map(results[0], (document) => document.toSavedSearchPartial());
			const pagedResult = new PagedResult(jsonObjects, new PageMetadata(page, _.size(jsonObjects), results[1]));

			return Promise.resolve(pagedResult);
		} catch (error) {
			return Promise.reject(new ServerError(error));
		}
	}

	public async create(savedSearch: ISavedSearch): Promise<ISavedSearch> {
		let savedSearchDocument: ISavedSearchDocument;

		try {
			savedSearchDocument = await this.SavedSearchModel.create(savedSearch);

			// Create is not atomic when isDefault is updated
			if (savedSearch.isDefault) {
				log.trace(`Start: Executing isDefault update query after creating document with id ${savedSearchDocument.id}`);
				await this.createIsDefaultUpdateQuery(savedSearchDocument).exec();
				log.trace(`Finish: Executing isDefault update query after creating document with id ${savedSearchDocument.id}`);
			}
		} catch (error) {
			return Promise.reject(new ServerError(error));
		}

		return Promise.resolve(savedSearchDocument.toSavedSearch());
	}

	public async update(updatedSavedSearch: ISavedSearch): Promise<ISavedSearch> {
		let savedSearchDocument: ISavedSearchDocument;
		let isDefaultChanged: boolean;

		try {
			// The following was excerpted from the mongoose documentation:
			// "Note that findByIdAndUpdate/Remove do not execute any hooks or validation
			// before making the change in the database.  However, if you need hooks and
			// full document validation first query for the document and then save() it."
			// The SavedSearchSchema uses mongoose hooks for date conversion to/from a string.
			// Therefore, save() is used here instead of update().
			savedSearchDocument = await this.SavedSearchModel.findById(updatedSavedSearch.id).exec();

			isDefaultChanged = updatedSavedSearch.isDefault !== savedSearchDocument.isDefault;

			_.assign(savedSearchDocument, updatedSavedSearch);

			savedSearchDocument = await savedSearchDocument.save();

			// Update is not atomic when isDefault is updated
			if (isDefaultChanged && updatedSavedSearch.isDefault) {
				log.trace(`Start: Executing isDefault update query after updating document with id ${savedSearchDocument.id}`);
				await this.createIsDefaultUpdateQuery(savedSearchDocument).exec();
				log.trace(`Finish: Executing isDefault update query after updating document with id ${savedSearchDocument.id}`);
			}
		} catch (error) {
			return Promise.reject(new ServerError(error));
		}

		return Promise.resolve(savedSearchDocument.toSavedSearch());
	}

	public async deleteById(id: string): Promise<ISavedSearch> {
		try {
			const savedSearch = await this.SavedSearchModel.findByIdAndRemove(id).exec();

			return Promise.resolve(savedSearch.toSavedSearch());
		} catch (error) {
			return Promise.reject(new ServerError(error));
		}
	}

	private createIsDefaultUpdateQuery(savedSearchDocument: ISavedSearchDocument): mongoose.Query<ISavedSearchDocument> {
		const conditions = {
			userEmail: savedSearchDocument.userEmail,
			criteriaId: savedSearchDocument.criteriaId,
			_id: { $ne: savedSearchDocument.id }
		};
		const update = { isDefault: false };
		const options = { multi: true };

		return this.SavedSearchModel.update(conditions, update, options);
	}
}
