import {PagedResult} from '../../models/PagedResult';
import {ISavedSearch} from '../models/ISavedSearch';
import {ISavedSearchPartial} from '../models/ISavedSearchPartial';
import {SavedSearchFilter} from '../models/SavedSearchFilter';

export interface ISavedSearchService {
	findById(id: string): Promise<ISavedSearch>;

	findByFilter(filter: SavedSearchFilter, page: number, pageSize: number): Promise<PagedResult<ISavedSearchPartial[]>>;

	create(savedSearch: ISavedSearch): Promise<ISavedSearch>;

	update(savedSearch: ISavedSearch): Promise<ISavedSearch>;

	deleteById(id: string): Promise<ISavedSearch>;
}
