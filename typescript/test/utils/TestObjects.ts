import {IUser} from 'cft-smart-security';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as TypeMoq from 'typemoq';
import * as uuid from 'uuid';
import {ISavedSearch} from '../../app/search/models/ISavedSearch';
import {ISavedSearchPartial} from '../../app/search/models/ISavedSearchPartial';
import {SavedSearchFilter} from '../../app/search/models/SavedSearchFilter';

const DATE_FORMAT = 'MMDDYYYY';

export class TestObjects {
	public static savedSearch(userEmail: string, isDefault: boolean = false): ISavedSearch {
		return _.assign(TestObjects.savedSearchPartial(userEmail, isDefault), {
			id: TestObjects.id(),
			startDate: moment().format(DATE_FORMAT),
			endDate: moment().format(DATE_FORMAT),
			timeFrame: TestObjects.randomString(),
			adStatus: TestObjects.randomString(),
			fields: [{
				name: TestObjects.randomString(),
				include: [TestObjects.randomString()],
				exclude: [TestObjects.randomString()]
			}]
		});
	}

	public static savedSearchPartial(userEmail: string, isDefault: boolean = false): ISavedSearchPartial {
		return {
			userEmail: userEmail,
			accountCode: TestObjects.randomString(),
			title: TestObjects.randomString(),
			isDefault: isDefault,
			criteriaId: TestObjects.randomString(),
			schemaNamespace: TestObjects.randomString()
		};
	}

	public static savedSearchFilter(userEmail: string): SavedSearchFilter {
		return {
			userEmail: userEmail,
			accountCode: TestObjects.randomString(),
			title: TestObjects.randomString(),
			isDefault: false,
			criteriaId: TestObjects.randomString(),
			schemaNamespace: TestObjects.randomString()
		};
	}

	public static user(userEmail: string, isAdmin: boolean): IUser {
		const mockUser = TypeMoq.Mock.ofType<IUser>(undefined, TypeMoq.MockBehavior.Loose);

		mockUser.setup((mock) => mock.email).returns(() => userEmail);
		mockUser.setup((mock) => mock.isAdmin).returns(() => isAdmin);

		return mockUser.object;
	}

	public static id(): string {
		return uuid().replace('-', '');
	}

	public static randomString(): string {
		return Math.random().toString(36).slice(2);
	}

	public static email(): string {
		return `${TestObjects.randomString()}@${TestObjects.randomString()}.com`;
	}
}
