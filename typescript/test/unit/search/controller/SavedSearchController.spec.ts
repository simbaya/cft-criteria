import {IUser} from 'cft-smart-security';
import * as chai from 'chai';
import {Request} from 'express';
import {Container} from 'inversify';
import * as _ from 'lodash';
import * as TypeMoq from 'typemoq';
import {NotFoundError, ServerError, UnauthorizedError, ValidationError} from '../../../../app/models/errors';
import {PagedResult} from '../../../../app/models/PagedResult';
import {PageMetadata} from '../../../../app/models/PageMetadata';
import {SmartResponse} from '../../../../app/models/SmartResponse';
import {ISavedSearchController} from '../../../../app/search/controller/ISavedSearchController';
import {SavedSearchController} from '../../../../app/search/controller/SavedSearchController';
import {ISavedSearch} from '../../../../app/search/models/ISavedSearch';
import {ISavedSearchPartial} from '../../../../app/search/models/ISavedSearchPartial';
import {SavedSearchFilter} from '../../../../app/search/models/SavedSearchFilter';
import {ISavedSearchService} from '../../../../app/search/service/ISavedSearchService';
import {SavedSearchServiceType} from '../../../../app/search/service/SavedSearchService';
import {TestObjects} from '../../../utils/TestObjects';

describe('Unit: SavedSearchController', (): void => {
	let container: Container;
	let mockSavedSearchService: TypeMoq.IMock<ISavedSearchService>;
	let mockRequest: TypeMoq.IMock<Request>;
	let savedSearchController: ISavedSearchController;
	let testSavedSearch: ISavedSearch;
	let testEmail: string;
	let testUser: IUser;
	let testAdminUser: IUser;
	let testId: string;
	let testServerError: Error;

	beforeEach((): void => {
		testEmail = TestObjects.email();
		testId = TestObjects.id();
		testUser = TestObjects.user(testEmail, false);
		testAdminUser = TestObjects.user(TestObjects.email(), true);
		testServerError = new ServerError(null);

		// By default, the test saved search is for the test user
		testSavedSearch = TestObjects.savedSearch(testEmail);

		mockSavedSearchService = TypeMoq.Mock.ofType<ISavedSearchService>(undefined, TypeMoq.MockBehavior.Loose);
		mockRequest = TypeMoq.Mock.ofType<Request>();
		mockRequest.setup((mock) => mock.requestId).returns(() => TestObjects.id());

		container = new Container();
		container.bind<ISavedSearchService>(SavedSearchServiceType).toConstantValue(mockSavedSearchService.object);
		container.bind<ISavedSearchController>(SavedSearchController).to(SavedSearchController);

		savedSearchController = container.get<ISavedSearchController>(SavedSearchController);
	});

	afterEach((): void => {
		mockSavedSearchService.reset();
		mockRequest.reset();
	});

	describe('create:', (): void => {
		beforeEach((): void => {
			testSavedSearch.id = null;
		});

		it('should create new saved search', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.create(TypeMoq.It.isValue(testSavedSearch)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.create(testSavedSearch, mockRequest.object)
				.then((smartResponse: SmartResponse<ISavedSearch>) => {
					mockSavedSearchService.verifyAll();
					chai.assert.isTrue(smartResponse.succeeded);
					chai.assert.deepEqual(testSavedSearch, smartResponse.result);
					done();
				})
				.catch((e) => done(e));
		});

		it('should allow admin to create a saved search for another user', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testAdminUser);

			mockSavedSearchService
				.setup((mock) => mock.create(TypeMoq.It.isValue(testSavedSearch)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.create(testSavedSearch, mockRequest.object)
				.then((smartResponse: SmartResponse<ISavedSearch>) => {
					mockSavedSearchService.verifyAll();
					chai.assert.isTrue(smartResponse.succeeded);
					chai.assert.deepEqual(testSavedSearch, smartResponse.result);
					done();
				})
				.catch((e) => done(e));
		});

		it('should return a server error if non-admin user email and saved search email don\'t match', (done: MochaDone): void => {
			const someOtherUser = TestObjects.user(TestObjects.email(), false);

			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => someOtherUser);

			mockSavedSearchService
				.setup((mock) => mock.create(TypeMoq.It.isValue(testSavedSearch)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			savedSearchController.create(testSavedSearch, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: UnauthorizedError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 404);
					chai.assert.equal(error.message, 'Unable to perform the requested action');
					done();
				});
		});

		it('should return server error if service rejects with one', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.create(TypeMoq.It.isValue(testSavedSearch)))
				.throws(testServerError)
				.verifiable(TypeMoq.Times.once());

			savedSearchController.create(testSavedSearch, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ServerError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 500);
					chai.assert.equal(error.message, 'A server error occurred');
					done();
				});
		});
	});

	describe('getById:', (): void => {
		it('should get saved search by id', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.getById(testId, mockRequest.object)
				.then((smartResponse: SmartResponse<ISavedSearch>) => {
					mockSavedSearchService.verifyAll();
					chai.assert.isTrue(smartResponse.succeeded);
					chai.assert.deepEqual(testSavedSearch, smartResponse.result);
					done();
				})
				.catch((e) => done(e));
		});

		it('should return validation error for non-alphanumeric id', (done: MochaDone): void => {
			const nonAlphaNumericId = `${testId}#`;

			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(nonAlphaNumericId)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			savedSearchController.getById(nonAlphaNumericId, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ValidationError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 403);
					chai.assert.equal(error.message, `ValidationError - ${nonAlphaNumericId} is not a valid id`);
					done();
				});
		});

		it('should return validation error for empty id', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue('')))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			savedSearchController.getById('', mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ValidationError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 403);
					chai.assert.equal(error.message, 'ValidationError - id parameter should not be empty');
					done();
				});
		});

		it('should return not found error for empty result', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.returns(() => Promise.resolve(null))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.getById(testId, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: NotFoundError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 404);
					chai.assert.equal(error.message, `Document with id ${testId} not found`);
					done();
				});
		});

		it('should return unauthorized error if saved search not owned by user', (done: MochaDone): void => {
			const someOtherUser = TestObjects.user(TestObjects.email(), false);

			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => someOtherUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.getById(testId, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: UnauthorizedError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 404);
					chai.assert.equal(error.message, 'Unable to perform the requested action');
					done();
				});
		});

		it('should allow admin user to get a saved search owned by another user', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testAdminUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.getById(testId, mockRequest.object)
				.then((smartResponse: SmartResponse<ISavedSearch>) => {
					mockSavedSearchService.verifyAll();
					chai.assert.isTrue(smartResponse.succeeded);
					chai.assert.deepEqual(testSavedSearch, smartResponse.result);
					done();
				})
				.catch((e) => done(e));
		});

		it('should return server error if service rejects with one', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.throws(testServerError)
				.verifiable(TypeMoq.Times.once());

			savedSearchController.getById(testId, mockRequest.object)
			.then(() => {
				done(new Error('Expected promise to be rejected'));
			})
			.catch((error: ServerError) => {
				mockSavedSearchService.verifyAll();
				chai.assert.equal(error.status, 500);
				chai.assert.equal(error.message, 'A server error occurred');
				done();
			});
		});
	});

	describe('deleteById:', (): void => {
		it('should delete saved search by id', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.deleteById(testId, mockRequest.object)
				.then((smartResponse: SmartResponse<ISavedSearch>) => {
					mockSavedSearchService.verifyAll();
					chai.assert.deepEqual(testSavedSearch, smartResponse.result);
					done();
				})
				.catch((e) => done(e));
		});

		it('should allow admin user to delete a saved search owned by another user', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testAdminUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.deleteById(testId, mockRequest.object)
				.then((smartResponse: SmartResponse<ISavedSearch>) => {
					mockSavedSearchService.verifyAll();
					chai.assert.deepEqual(testSavedSearch, smartResponse.result);
					done();
				})
				.catch((e) => done(e));
		});

		it('should return validation error for non-alphanumeric id', (done: MochaDone): void => {
			const nonAlphaNumericId = `${testId}#`;

			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(nonAlphaNumericId)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			savedSearchController.deleteById(nonAlphaNumericId, mockRequest.object)
				.then(() => {
				  done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ValidationError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 403);
					chai.assert.equal(error.message, `ValidationError - ${nonAlphaNumericId} is not a valid id`);
					done();
				});
		});

		it('should return validation error for empty id', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue('')))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			savedSearchController.deleteById('', mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ValidationError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 403);
					chai.assert.equal(error.message, 'ValidationError - id parameter should not be empty');
					done();
				});
		});

		it('should return not found error for empty result', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.returns(() => Promise.resolve(null))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.deleteById(testId, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: NotFoundError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 404);
					chai.assert.equal(error.message, `Document with id ${testId} not found`);
					done();
				});
		});

		it('should return unauthorized error if saved search not owned by user', (done: MochaDone): void => {
			const someOtherUser = TestObjects.user(TestObjects.email(), false);

			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => someOtherUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.deleteById(testId, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: UnauthorizedError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 404);
					chai.assert.equal(error.message, 'Unable to perform the requested action');
					done();
				});
		});

		it('should return server error if service rejects with one', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.throws(testServerError)
				.verifiable(TypeMoq.Times.once());

			savedSearchController.deleteById(testId, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ServerError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 500);
					chai.assert.equal(error.message, 'A server error occurred');
					done();
				});
		});
	});

	describe('findByFilter:', (): void => {
		let page: number;
		let pageSize: number;
		let totalCount: number;
		let filter: SavedSearchFilter;
		let pageMetadata: PageMetadata;
		let pagedResult: PagedResult<ISavedSearchPartial[]>;
		let savedSearchPartial: ISavedSearchPartial;

		beforeEach((): void => {
			page = 1;
			pageSize = 5;
			totalCount = 13;
			filter = TestObjects.savedSearchFilter(testEmail);
			savedSearchPartial = TestObjects.savedSearchPartial(testEmail);
			pageMetadata = new PageMetadata(page, pageSize, totalCount);
			pagedResult = new PagedResult(_.fill(Array(pageSize), savedSearchPartial), pageMetadata);
		});

		it('should find by filter', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findByFilter(
					TypeMoq.It.isValue(filter),
					TypeMoq.It.isValue(page),
					TypeMoq.It.isValue(pageSize))
				)
				.returns(() => Promise.resolve(pagedResult))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.findByFilter(filter, page, pageSize, mockRequest.object)
				.then((smartResponse: SmartResponse<ISavedSearchPartial[]>) => {
					mockSavedSearchService.verifyAll();
					chai.assert.isTrue(smartResponse.succeeded);
					chai.assert.deepEqual(pagedResult.result, smartResponse.result);
					chai.assert.deepEqual(pagedResult.pageMetadata, smartResponse.paging);
					done();
				})
				.catch((e) => done(e));
		});

		it('should allow admin to find saved searches owned by another user', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testAdminUser);

			mockSavedSearchService
				.setup((mock) => mock.findByFilter(
					TypeMoq.It.isValue(filter),
					TypeMoq.It.isValue(page),
					TypeMoq.It.isValue(pageSize))
				)
				.returns(() => Promise.resolve(pagedResult))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.findByFilter(filter, page, pageSize, mockRequest.object)
				.then((smartResponse: SmartResponse<ISavedSearchPartial[]>) => {
					mockSavedSearchService.verifyAll();
					chai.assert.isTrue(smartResponse.succeeded);
					chai.assert.deepEqual(pagedResult.result, smartResponse.result);
					chai.assert.deepEqual(pagedResult.pageMetadata, smartResponse.paging);
					done();
				})
				.catch((e) => done(e));
		});

		it('should return unauthorized error if filter email and non-admin user email don\'t match', (done: MochaDone): void => {
			const someOtherUser = TestObjects.user(TestObjects.email(), false);

			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => someOtherUser);

			mockSavedSearchService
				.setup((mock) => mock.findByFilter(
					TypeMoq.It.isValue(filter),
					TypeMoq.It.isValue(page),
					TypeMoq.It.isValue(pageSize))
				)
				.throws(testServerError)
				.verifiable(TypeMoq.Times.never());

			savedSearchController.findByFilter(filter, page, pageSize, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: UnauthorizedError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 404);
					chai.assert.equal(error.message, 'Unable to perform the requested action');
					done();
				});
		});

		it('should return server error if service rejects with one', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findByFilter(
					TypeMoq.It.isValue(filter),
					TypeMoq.It.isValue(page),
					TypeMoq.It.isValue(pageSize))
				)
				.throws(testServerError)
				.verifiable(TypeMoq.Times.once());

			savedSearchController.findByFilter(filter, page, pageSize, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ServerError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 500);
					chai.assert.equal(error.message, 'A server error occurred');
					done();
				});
		});
	});

	describe('update:', (): void => {
		it('should update document', (done: MochaDone): void => {
			const updatedSavedSearch = TestObjects.savedSearch(testEmail);
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testSavedSearch.id)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.once());

			mockSavedSearchService
				.setup((mock) => mock.update(TypeMoq.It.isValue(testSavedSearch)))
				.returns(() => Promise.resolve(updatedSavedSearch))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.update(testSavedSearch.id, testSavedSearch, mockRequest.object)
				.then((smartResponse) => {
					mockSavedSearchService.verifyAll();
					chai.assert.isTrue(smartResponse.succeeded);
					chai.assert.deepEqual(updatedSavedSearch, smartResponse.result);
					done();
				})
				.catch((e) => done(e));
		});

		it('should return server error if service rejects with one', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testSavedSearch.id)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.once());

			mockSavedSearchService
				.setup((mock) => mock.update(TypeMoq.It.isValue(testSavedSearch)))
				.throws(testServerError)
				.verifiable(TypeMoq.Times.once());

			savedSearchController.update(testSavedSearch.id, testSavedSearch, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ServerError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 500);
					chai.assert.equal(error.message, 'A server error occurred');
					done();
				});
		});

		it('should return validation error for non-alphanumeric id', (done: MochaDone): void => {
			const nonAlphaNumericId = `${testId}#`;

			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(nonAlphaNumericId)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			mockSavedSearchService
				.setup((mock) => mock.update(TypeMoq.It.isValue(testSavedSearch)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			savedSearchController.update(nonAlphaNumericId, testSavedSearch, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ValidationError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 403);
					chai.assert.equal(error.message, `ValidationError - ${nonAlphaNumericId} is not a valid id`);
					done();
				});
		});

		it('should return validation error for empty id', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue('')))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			mockSavedSearchService
				.setup((mock) => mock.update(TypeMoq.It.isValue(testSavedSearch)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			savedSearchController.update('', testSavedSearch, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ValidationError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 403);
					chai.assert.equal(error.message, 'ValidationError - id parameter should not be empty');
					done();
				});
		});

		it('should return validation error if ids in URL and body don\'t match', (done: MochaDone): void => {
			const otherId = TestObjects.id();

			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(otherId)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			mockSavedSearchService
				.setup((mock) => mock.update(TypeMoq.It.isValue(testSavedSearch)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			savedSearchController.update(otherId, testSavedSearch, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ValidationError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 403);
					chai.assert.equal(error.message, 'ValidationError - The ids in the URL and body must match');
					done();
				});
		});

		it('should return not found error for empty result', (done: MochaDone): void => {
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testSavedSearch.id)))
				.returns(() => Promise.resolve(null))
				.verifiable(TypeMoq.Times.once());

			mockSavedSearchService
				.setup((mock) => mock.update(TypeMoq.It.isValue(testSavedSearch)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			savedSearchController.update(testSavedSearch.id, testSavedSearch, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: NotFoundError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 404);
					chai.assert.equal(error.message, `Document with id ${testSavedSearch.id} not found`);
					done();
				});
		});

		it('should return authorization error if non-admin user email and updated saved search email don\'t match', (done: MochaDone): void => {
			const someOtherUser = TestObjects.user(TestObjects.email(), false);

			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => someOtherUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testSavedSearch.id)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			mockSavedSearchService
				.setup((mock) => mock.update(TypeMoq.It.isValue(testSavedSearch)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			savedSearchController.update(testSavedSearch.id, testSavedSearch, mockRequest.object)
				.then(() => {
				  done(new Error('Expected promise to be rejected'));
				})
				.catch((error: UnauthorizedError) => {
				  mockSavedSearchService.verifyAll();
				  chai.assert.equal(error.status, 404);
				  chai.assert.equal(error.message, 'Unable to perform the requested action');
				  done();
				});
		});

		it('should return unauthorized error if saved search not owned by user', (done: MochaDone): void => {
			const existingSavedSearch = TestObjects.savedSearch(TestObjects.email());

			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testSavedSearch.id)))
				.returns(() => Promise.resolve(existingSavedSearch))
				.verifiable(TypeMoq.Times.once());

			mockSavedSearchService
				.setup((mock) => mock.update(TypeMoq.It.isValue(testSavedSearch)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.never());

			savedSearchController.update(testSavedSearch.id, testSavedSearch, mockRequest.object)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: UnauthorizedError) => {
					mockSavedSearchService.verifyAll();
					chai.assert.equal(error.status, 404);
					chai.assert.equal(error.message, 'Unable to perform the requested action');
					done();
				});
		});

		it('should allow admin user to update a saved search owned by another user', (done: MochaDone): void => {
			const updatedSavedSearch = TestObjects.savedSearch(testEmail);
			mockRequest.setup((mock) => mock.authenticatedUser).returns(() => testAdminUser);

			mockSavedSearchService
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testSavedSearch.id)))
				.returns(() => Promise.resolve(testSavedSearch))
				.verifiable(TypeMoq.Times.once());

			mockSavedSearchService
				.setup((mock) => mock.update(TypeMoq.It.isValue(testSavedSearch)))
				.returns(() => Promise.resolve(updatedSavedSearch))
				.verifiable(TypeMoq.Times.once());

			savedSearchController.update(testSavedSearch.id, testSavedSearch, mockRequest.object)
				.then((smartResponse) => {
					mockSavedSearchService.verifyAll();
					chai.assert.isTrue(smartResponse.succeeded);
					chai.assert.deepEqual(updatedSavedSearch, smartResponse.result);
					done();
				})
				.catch((e) => done(e));
		});
	});
});
