import * as chai from 'chai';
import {Container} from 'inversify';
import * as _ from 'lodash';
import {DocumentQuery, Model, Query} from 'mongoose';
import * as TypeMoq from 'typemoq';
import {ServerError} from '../../../../app/models/errors/ServerError';
import {PagedResult} from '../../../../app/models/PagedResult';
import {PageMetadata} from '../../../../app/models/PageMetadata';
import {ISavedSearch} from '../../../../app/search/models/ISavedSearch';
import {ISavedSearchPartial} from '../../../../app/search/models/ISavedSearchPartial';
import {SavedSearchFilter} from '../../../../app/search/models/SavedSearchFilter';
import {ISavedSearchDocument} from '../../../../app/search/models/SavedSearchModel';
import {ISavedSearchModelFactory, SavedSearchModelFactoryType} from '../../../../app/search/models/SavedSearchModelFactory';
import {SavedSearchPartial} from '../../../../app/search/models/SavedSearchPartial';
import {ISavedSearchService} from '../../../../app/search/service/ISavedSearchService';
import {SavedSearchService, SavedSearchServiceType} from '../../../../app/search/service/SavedSearchService';
import {TestObjects} from '../../../utils/TestObjects';

describe('Unit: SavedSearchService', (): void => {
	let testId: string;
	let testEmail: string;
	let testSavedSearch: ISavedSearch;
	let testError: Error;
	let mockModel: TypeMoq.IMock<Model<ISavedSearchDocument>>;
	let mockQuery: TypeMoq.IMock<Query<ISavedSearchDocument>>;
	let mockDocument: TypeMoq.IMock<ISavedSearchDocument>;
	let mockModelFactory: TypeMoq.IMock<ISavedSearchModelFactory>;
	let container: Container;
	let savedSearchService: ISavedSearchService;

	beforeEach((): void => {
		testEmail = TestObjects.email();
		testSavedSearch = TestObjects.savedSearch(testEmail);
		testId = testSavedSearch.id;
		testError = new Error('Mongoose error');

		mockModelFactory = TypeMoq.Mock.ofType<ISavedSearchModelFactory>(undefined, TypeMoq.MockBehavior.Loose);
		mockModel = TypeMoq.Mock.ofType<Model<ISavedSearchDocument>>(undefined, TypeMoq.MockBehavior.Loose);
		mockQuery = TypeMoq.Mock.ofType<Query<ISavedSearchDocument>>(undefined, TypeMoq.MockBehavior.Loose);
		mockDocument = TypeMoq.Mock.ofType<ISavedSearchDocument>(undefined, TypeMoq.MockBehavior.Loose);

		// TypeMoq has some weirdness when returning a resolved promise that is a TypeMoq object
		// because of how they wrap things when doing a dynamic mock.
		// https://github.com/florinn/typemoq/issues/66
		mockQuery.setup((mock: any) => mock.then).returns(() => undefined);
		mockDocument.setup((mock: any) => mock.then).returns(() => undefined);

		mockDocument
			.setup((mock) => mock.isDefault)
			.returns(() => testSavedSearch.isDefault);

		mockDocument
			.setup((mock) => mock.toSavedSearch())
			.returns(() => testSavedSearch);

		mockModelFactory
			.setup((mock) => mock.getSavedSearchModel())
			.returns(() => mockModel.object);

		container = new Container();
		container.bind<ISavedSearchModelFactory>(SavedSearchModelFactoryType).toConstantValue(mockModelFactory.object);
		container.bind<ISavedSearchService>(SavedSearchServiceType).to(SavedSearchService);

		savedSearchService = container.get<ISavedSearchService>(SavedSearchServiceType);
	});

	afterEach((): void => {
		mockModel.reset();
		mockQuery.reset();
		mockDocument.reset();
		mockModelFactory.reset();
	});

	describe('findById:', () => {
		beforeEach((): void => {
			mockModel
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.returns(() => mockQuery.object)
				.verifiable(TypeMoq.Times.once());
		});

		afterEach((): void => {
			mockModel.reset();
		});

		it('should find by id', (done: MochaDone): void => {
			mockQuery
				.setup((mock) => mock.exec())
				.returns(() => Promise.resolve(mockDocument.object))
				.verifiable(TypeMoq.Times.once());

			savedSearchService.findById(testId)
				.then((savedSearch: ISavedSearch) => {
					mockModel.verifyAll();
					mockQuery.verifyAll();
					chai.assert.deepEqual(savedSearch, testSavedSearch);
					done();
				})
				.catch((e) => done(e));
		});

		it('should resolve null if not found', (done: MochaDone): void => {
			mockQuery
				.setup((mock) => mock.exec())
				.returns(() => Promise.resolve(null))
				.verifiable(TypeMoq.Times.once());

			savedSearchService.findById(testId)
				.then((savedSearch: ISavedSearch) => {
					mockModel.verifyAll();
					mockQuery.verifyAll();
					chai.assert.isNull(savedSearch);
					done();
				})
				.catch((e) => done(e));
		});

		it('should reject on error', (done: MochaDone): void => {
			mockQuery
				.setup((mock) => mock.exec())
				.throws(testError)
				.verifiable(TypeMoq.Times.once());

			savedSearchService.findById(testId)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ServerError) => {
					mockModel.verifyAll();
					mockQuery.verifyAll();
					chai.assert.deepEqual(error.originalError, testError);
					done();
				});
		});
	});

	describe('create:', () => {
		it('should create new document', (done: MochaDone): void => {
			mockModel
				.setup((mock) => mock.create(TypeMoq.It.isValue(testSavedSearch)))
				.returns(() => Promise.resolve(mockDocument.object))
				.verifiable(TypeMoq.Times.once());

			savedSearchService.create(testSavedSearch)
				.then((savedSearch: ISavedSearch) => {
					mockModel.verifyAll();
					chai.assert.deepEqual(testSavedSearch, savedSearch);
					done();
				})
				.catch((e) => done(e));
		});

		it('should reject on error', (done: MochaDone): void => {
			mockModel
				.setup((mock) => mock.create(TypeMoq.It.isValue(testSavedSearch)))
				.throws(testError)
				.verifiable(TypeMoq.Times.once());

			savedSearchService.create(testSavedSearch)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ServerError) => {
					mockModel.verifyAll();
					chai.assert.deepEqual(error.originalError, testError);
					done();
				});
		});

		it('should set isDefault flag to false for non-default saved searches', (done: MochaDone): void => {
			const mockCreatedDocument = TypeMoq.Mock.ofType<ISavedSearchDocument>(undefined, TypeMoq.MockBehavior.Loose);
			const newSavedSearch = TestObjects.savedSearch(testEmail, true);
			const conditions = {
				userEmail: newSavedSearch.userEmail,
				criteriaId: newSavedSearch.criteriaId,
				_id: { $ne: newSavedSearch.id }
			};

			mockCreatedDocument.setup((mock: any) => mock.then).returns(() => undefined);
			mockCreatedDocument.setup((mock) => mock.isDefault).returns(() => newSavedSearch.isDefault);
			mockCreatedDocument.setup((mock) => mock.userEmail).returns(() => newSavedSearch.userEmail);
			mockCreatedDocument.setup((mock) => mock.criteriaId).returns(() => newSavedSearch.criteriaId);
			mockCreatedDocument.setup((mock) => mock.id).returns(() => newSavedSearch.id);

			mockCreatedDocument
				.setup((mock) => mock.toSavedSearch())
				.returns(() => newSavedSearch)
				.verifiable(TypeMoq.Times.once());

			mockQuery
				.setup((mock) => mock.exec())
				.returns(() => Promise.resolve(mockCreatedDocument.object))
				.verifiable(TypeMoq.Times.once());

			mockModel
				.setup((mock) => mock.create(TypeMoq.It.isValue(newSavedSearch)))
				.returns(() => Promise.resolve(mockCreatedDocument.object))
				.verifiable(TypeMoq.Times.once());

			mockModel
				.setup((mock) => mock.update(
					TypeMoq.It.isValue(conditions),
					TypeMoq.It.isValue({ isDefault: false }),
					TypeMoq.It.isValue({ multi: true }))
				)
				.returns(() => mockQuery.object)
				.verifiable(TypeMoq.Times.once());

			savedSearchService.create(newSavedSearch)
				.then((savedSearch: ISavedSearch) => {
					mockModel.verifyAll();
					mockQuery.verifyAll();
					mockCreatedDocument.verifyAll();
					chai.assert.deepEqual(newSavedSearch, savedSearch);
					done();
				})
				.catch((e) => done(e));
		});
	});

	describe('update:', () => {
		it('should update document', (done: MochaDone): void => {
			mockModel
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.returns(() => mockQuery.object)
				.verifiable(TypeMoq.Times.once());

			mockDocument
				.setup((mock) => mock.save())
				.returns(() => Promise.resolve(mockDocument.object))
				.verifiable(TypeMoq.Times.once());

			mockQuery
			  .setup((mock) => mock.exec())
			  .returns(() => Promise.resolve(mockDocument.object))
			  .verifiable(TypeMoq.Times.once());

			savedSearchService.update(testSavedSearch)
				.then((savedSearch: ISavedSearch) => {
					mockModel.verifyAll();
					mockQuery.verifyAll();
					mockDocument.verifyAll();
					chai.assert.deepEqual(testSavedSearch, savedSearch);
					done();
				})
				.catch((e) => done(e));
		});

		it('should reject on error', (done: MochaDone): void => {
			mockModel
				.setup((mock) => mock.findById(TypeMoq.It.isValue(testId)))
				.returns(() => mockQuery.object)
				.verifiable(TypeMoq.Times.once());

			mockQuery
				.setup((mock) => mock.exec())
				.throws(testError)
				.verifiable(TypeMoq.Times.once());

			savedSearchService.update(testSavedSearch)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ServerError) => {
					mockModel.verifyAll();
					mockQuery.verifyAll();
					chai.assert.deepEqual(error.originalError, testError);
					done();
				});
		});

		it('should set isDefault flag to false for non-default saved searches', (done: MochaDone): void => {
			const mockUpdatedDocument = TypeMoq.Mock.ofType<ISavedSearchDocument>(undefined, TypeMoq.MockBehavior.Loose);
			const updatedSavedSearch = TestObjects.savedSearch(testEmail, true);
			const conditions = {
				userEmail: updatedSavedSearch.userEmail,
				criteriaId: updatedSavedSearch.criteriaId,
				_id: { $ne: updatedSavedSearch.id }
			};

			mockUpdatedDocument.setup((mock: any) => mock.then).returns(() => undefined);
			mockUpdatedDocument.setup((mock) => mock.isDefault).returns(() => false);
			mockUpdatedDocument.setup((mock) => mock.userEmail).returns(() => updatedSavedSearch.userEmail);
			mockUpdatedDocument.setup((mock) => mock.criteriaId).returns(() => updatedSavedSearch.criteriaId);
			mockUpdatedDocument.setup((mock) => mock.id).returns(() => updatedSavedSearch.id);

			mockUpdatedDocument
				.setup((mock) => mock.toSavedSearch())
				.returns(() => updatedSavedSearch)
				.verifiable(TypeMoq.Times.once());

			mockUpdatedDocument
				.setup((mock) => mock.save())
				.returns(() => Promise.resolve(mockUpdatedDocument.object))
				.verifiable(TypeMoq.Times.once());

			mockQuery
				.setup((mock) => mock.exec())
				.returns(() => Promise.resolve(mockUpdatedDocument.object))
				.verifiable(TypeMoq.Times.exactly(2));

			mockModel
				.setup((mock) => mock.findById(TypeMoq.It.isValue(updatedSavedSearch.id)))
				.returns(() => mockQuery.object)
				.verifiable(TypeMoq.Times.once());

			mockModel
				.setup((mock) => mock.update(
					TypeMoq.It.isValue(conditions),
					TypeMoq.It.isValue({ isDefault: false }),
					TypeMoq.It.isValue({ multi: true }))
				)
				.returns(() => mockQuery.object)
				.verifiable(TypeMoq.Times.once());

			savedSearchService.update(updatedSavedSearch)
				.then((savedSearch: ISavedSearch) => {
					mockModel.verifyAll();
					mockQuery.verifyAll();
					mockUpdatedDocument.verifyAll();
					chai.assert.deepEqual(updatedSavedSearch, savedSearch);
					done();
				})
				.catch((e) => done(e));
		});
	});

	describe('deleteById:', () => {
		beforeEach((): void => {
			mockModel
				.setup((mock) => mock.findByIdAndRemove(TypeMoq.It.isValue(testId)))
				.returns(() => mockQuery.object)
				.verifiable(TypeMoq.Times.once());
		});

		afterEach((): void => {
			mockModel.reset();
		});

		it('should delete by id', (done: MochaDone): void => {
			mockQuery
				.setup((mock) => mock.exec())
				.returns(() => Promise.resolve(mockDocument.object))
				.verifiable(TypeMoq.Times.once());

			savedSearchService.deleteById(testId)
				.then((savedSearch: ISavedSearch) => {
					mockModel.verifyAll();
					mockQuery.verifyAll();
					chai.assert.deepEqual(savedSearch, testSavedSearch);
					done();
				})
				.catch((e) => done(e));
		});

		it('should reject on error', (done: MochaDone): void => {
			mockQuery
				.setup((mock) => mock.exec())
				.throws(testError)
				.verifiable(TypeMoq.Times.once());

			savedSearchService.deleteById(testId)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ServerError) => {
					mockModel.verifyAll();
					mockQuery.verifyAll();
					chai.assert.deepEqual(error.originalError, testError);
					done();
				});
		});
	});

	describe('findByFilter:', () => {
		let testFilter: SavedSearchFilter;
		let mockCountQuery: TypeMoq.IMock<Query<number>>;
		let mockDocumentQuery: TypeMoq.IMock<DocumentQuery<ISavedSearchDocument[], ISavedSearchDocument>>;
		let savedSearchPartial: ISavedSearchPartial;
		let totalCount: number;
		let page: number;
		let pageSize: number;
		let select: string;

		beforeEach((): void => {
			savedSearchPartial = TestObjects.savedSearchPartial(testEmail);
			testFilter = TestObjects.savedSearchFilter(testEmail);
			select = _.keys(new SavedSearchPartial()).join(' ');
			totalCount = 13;
			page = 2;
			pageSize = 5;

			// Setup document partial method
			mockDocument
				.setup((mock) => mock.toSavedSearchPartial())
				.returns(() => savedSearchPartial);

			// Setup mock that gets the count of matching documents
			mockCountQuery = TypeMoq.Mock.ofType<Query<number>>(undefined, TypeMoq.MockBehavior.Loose);

			mockCountQuery.setup((mock: any) => mock.then).returns(() => undefined);

			// Setup mock that finds matching documents and pages them
			mockDocumentQuery = TypeMoq.Mock.ofType<DocumentQuery<ISavedSearchDocument[], ISavedSearchDocument>>(undefined, TypeMoq.MockBehavior.Loose);

			mockDocumentQuery.setup((mock: any) => mock.then).returns(() => undefined);

			mockDocumentQuery.setup((mock) => mock.select(TypeMoq.It.isValue(select)))
				.returns(() => mockDocumentQuery.object)
				.verifiable(TypeMoq.Times.once());

			mockDocumentQuery.setup((mock) => mock.sort(TypeMoq.It.isValue({_id: 1})))
				.returns(() => mockDocumentQuery.object)
				.verifiable(TypeMoq.Times.once());

			mockDocumentQuery.setup((mock) => mock.skip(TypeMoq.It.isValue((page - 1) * pageSize)))
				.returns(() => mockDocumentQuery.object)
				.verifiable(TypeMoq.Times.once());

			mockDocumentQuery.setup((mock) => mock.limit(TypeMoq.It.isValue(pageSize)))
				.returns(() => mockDocumentQuery.object)
				.verifiable(TypeMoq.Times.once());

			mockDocumentQuery
				.setup((mock) => mock.exec())
				.returns(() => Promise.resolve(_.fill(Array(pageSize), mockDocument.object)))
				.verifiable(TypeMoq.Times.once());

			// Setup document model to return the mock queries when find and count are called
			mockModel
				.setup((mock) => mock.count(TypeMoq.It.isValue(testFilter)))
				.returns(() => mockCountQuery.object)
				.verifiable(TypeMoq.Times.once());

			mockModel
				.setup((mock) => mock.find(TypeMoq.It.isValue(testFilter)))
				.returns(() => mockDocumentQuery.object)
				.verifiable(TypeMoq.Times.once());
		});

		afterEach((): void => {
			mockCountQuery.reset();
			mockDocumentQuery.reset();
			mockModel.reset();
		});

		it('should filter documents', (done: MochaDone): void => {
			mockCountQuery
				.setup((mock) => mock.exec())
				.returns(() => Promise.resolve(totalCount))
				.verifiable(TypeMoq.Times.once());

			savedSearchService.findByFilter(testFilter, page, pageSize)
				.then((pagedResult: PagedResult<ISavedSearchPartial[]>) => {
					mockModel.verifyAll();
					mockCountQuery.verifyAll();
					mockDocumentQuery.verifyAll();

					const expectedResult = _.fill(Array(pageSize), savedSearchPartial);
					const expectedPageMetadata = new PageMetadata(page, pageSize, totalCount);

					chai.assert.deepEqual(pagedResult.pageMetadata, expectedPageMetadata);
					chai.assert.deepEqual(pagedResult.result, expectedResult);

					done();
				})
				.catch((e) => done(e));
		});

		it('should reject on error', (done: MochaDone): void => {
			mockCountQuery
				.setup((mock) => mock.exec())
				.throws(testError)
				.verifiable(TypeMoq.Times.once());

			savedSearchService.findByFilter(testFilter, page, pageSize)
				.then(() => {
					done(new Error('Expected promise to be rejected'));
				})
				.catch((error: ServerError) => {
					mockCountQuery.verifyAll();
					chai.assert.deepEqual(error.originalError, testError);
					done();
				});
		});
	});
});
