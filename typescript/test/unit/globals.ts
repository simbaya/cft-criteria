import 'reflect-metadata';

// Set up source map
import sourceMapSupport = require('source-map-support');
sourceMapSupport.install();

process.env.NODE_ENV = 'test';
