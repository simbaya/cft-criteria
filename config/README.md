# Nodejs Configs
Config files are named with one of the environment values listed below

## Environments
 * dev
 * beta
 * prod

## Server Configs

### Example
```json
{
  "servers": {}
}
```

### Using

##### config.dev.json
```json
{
    "someValue": "value",
    "servers": {
        "reporting": {
            "scheme": "http",
            "host": "dev.competitrack.com",
            "port": 8000,
            "path": "/repository/api/v1"
        },
        "mongodb": [
          {
            "host": "webdev-mongodb-cluster01",
            "port": 37017,
            "path": "/test"
          }
        ]
    }
}
```

```typescript
import {Config, IServerConfig} from 'cft-smart-security';

// Getting multiple server
const mongoConfig = Config.getServerConfigs('mongodb');

// Getting a single server
const reportingConfig = Config.getServerConfig('reporting');

// Getting a non server config value
const someValue = Config.getConfig('someValue');
```

## PM2 Configs

### Example
```json
{
  "name" : "criteria-service",
  "script" : "./bin/server.js",
  "cwd" : "/var/www/criteria/current/",
  "exec_interpreter" : "node",
  "exec_mode": "cluster_mode",
  "env": {
    "NODE_ENV": "beta",
    "PORT": 5040
  },
  "out_file": "/var/log/competitrack/criteria-service/out.log",
  "access_file": "/var/log/competitrack/criteria-service/access.log",
  "error_file": "/var/log/competitrack/criteria-service/error.log",
  "pid_file": "/var/run/competitrack/criteria-service.pid",
  "instances": "2",
  "merge_logs": true
}
```

### Best Practices
 * Use 2 instances in cluster_mode to allow it to start / restart safely
 * Ensure the port is uniq across all other apps!

### Using pm2
Place info here
